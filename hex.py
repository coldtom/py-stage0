#!/usr/bin/python3

# Hex0 Assembler written in python
# Logic copied from hex assembler in stage0

import sys
import os             # Needed to change mode on stdout
from ctypes import *  # Needed to coerce correct byte representation

fp = os.fdopen(sys.stdout.fileno(), 'wb')

# Remove line comments
def purge_line(line):
    return line.split('#')[0].strip()

# Convert char into bytecode
def hex(char):
    if char >= '0' and char <= '9':
        return ord(char) - 48
    elif char >= 'a' and char <= 'z':
        return ord(char) - 87
    elif char >= 'A' and char <= 'Z':
        return ord(char) - 55
    else:
        raise ValueError("Called a hex function without a hex value")

# Main assembler
def assemble(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()

    full_file = ''.join([purge_line(line) for line in contents])

    hex_sum = 0
    toggle = False

    for char in full_file:
        if (char >= '0' and char <= '9') or (char >= 'a' and char <= 'z') or \
        (char >= 'A' and char <= 'Z') and (char != None):
            if not toggle:
                hex_sum = hex(char)
                toggle = True
            else:
                hex_sum = (hex_sum << 4) + hex(char)
                toggle = False
                fp.write(c_char(hex_sum).value)

if __name__ == '__main__':
    assemble(sys.argv[1])
