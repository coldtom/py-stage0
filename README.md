# py-stage0

An implementation of the [stage0
bootstrap](https://github.com/oriansj/stage0) tooling, written in python.
Hopefully will be converted to BuildStream plugins to allow building a
self-bootstrapping system.
